package main

import (
	"github.com/gorilla/handlers"
	"io/ioutil"
	"net/http"
	"text/template"
)

type Website struct {
	Titel  string
	Inhalt string
	test   string
}

func main() {
	router := http.NewServeMux()
	router.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("css"))))
	router.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("js"))))
	router.HandleFunc("/", startseite)
	http.ListenAndServe(":8080", handlers.CompressHandler(router))
	//	http.Serve(autocert.NewListener("webprogrammierung.org", "webassemblydemo.de"), handlers.CompressHandler(router))

}

func startseite(w http.ResponseWriter, r *http.Request) {
	var view, _ = template.ParseGlob("view/*")

	var website = Website{}
	Seite := r.URL.Path[len("/"):]
	if Seite == "" {
		Seite = "index"
	}
	text, _ := ioutil.ReadFile("docs/" + Seite + ".html")
	website.Inhalt = string(text)
	website.Titel = Seite
	view.ExecuteTemplate(w, "index.html", website)

}
